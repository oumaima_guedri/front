import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {
  id:number =0;
  us: any={'nom':'','prenom':'', 'datenaiss':'', 'email':'', 'telephone':'', 'poids':''};

  constructor(private serviceuser:UserCRUDService,private router:Router,private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.us=JSON.parse(params.special);
 }


      })
   
  }

  ngOnInit(): void {
  }

  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/listuser']);

  }
  modif():void{
    this.serviceuser.update(this.us).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/listuser'])

     },

     error: (u:any)=> console.error(u),

     complete:()=>{}

     })

  }

}
