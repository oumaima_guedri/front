import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeactComponent } from './listeact.component';

const routes: Routes = [
  {path:'',component:ListeactComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListeactRoutingModule { }
